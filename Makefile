STACK_NAME=image-labeling-dev
BUCKET_NAME=serabalint-euw1-sam-deployments
REGION=eu-west-1

.PHONY: deploy

install:
	cd recognition; \
	npm install; 
	cd add-label; \
	rm -rf node_modules; docker run --rm -v `pwd`:/var/task lambci/lambda:build-nodejs8.10 npm install;

test: install
	cd recognition; \
	npm test;

	cd add-label; \
	#npm test; #local
	docker run -it --rm -v `pwd`:/var/task --entrypoint /bin/bash lambci/lambda:nodejs8.10 -c 'npm test'

bucket:
	aws s3 mb s3://$(BUCKET_NAME) --region $(REGION)

deploy:
	# before deploy, remove dev dependencies
	cd recognition; npm prune --production;
	# rebuild is needed to rebuild binary packages to the target asw OS
	cd add-label; docker run --rm -v `pwd`:/var/task lambci/lambda:build-nodejs8.10 npm prune --production && npm rebuild;
	sam package --template-file template.yaml --output-template-file packaged.yaml --s3-bucket $(BUCKET_NAME)
	sam deploy --template-file packaged.yaml --stack-name $(STACK_NAME) --capabilities CAPABILITY_IAM --region $(REGION)
	

destroy:
	aws cloudformation delete-stack --stack-name $(STACK_NAME) --region $(REGION)
	aws s3 rb s3://$(BUCKET_NAME) --force --region $(REGION)

invoke:
   cd add-label; cat test_events/event.json| docker run -i -e DOCKER_LAMBDA_USE_STDIN=1 -e RESULT_BUCKET='labeling-labels-results-dev' -e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID -e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY -e AWS_LAMBDA_FUNCTION_TIMEOUT=100 --rm -v `pwd`:/var/task lambci/lambda:nodejs8.10

