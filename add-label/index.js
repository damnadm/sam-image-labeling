"use strict";
const headers = {
  "Access-Control-Allow-Headers":
    "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
  "Access-Control-Allow-Methods": "GET,OPTIONS",
  "Access-Control-Allow-Origin": "*"
};

const EventFilter = require("./model/EventFilter");
const LabelledImage = require("./model/LabelledImage");

exports.handler = async (event, context) => {
  try {
    const fileAndLabels = new EventFilter(event);
    console.log("label event details", fileAndLabels.message);
    const labels = [];
    for (let labelData of fileAndLabels.message.labels.Labels) {
      labels.push(labelData.Name);
    }
    const labelledImage = new LabelledImage(
      fileAndLabels.message.bucket,
      fileAndLabels.message.key,
      labels
    );

    await labelledImage.generateAndUpload();

    console.log("done");
    return {
      statusCode: 200,
      body: labels,
      headers
    };
  } catch (err) {
    console.log("AddLabel errors", err);
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify(err)
    };
  }
};
