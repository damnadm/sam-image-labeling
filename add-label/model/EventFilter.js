class EventFilter {
  constructor(event) {
    this.event = event;
    this.message = null;
    this.__getParams();
  }

  __getParams() {
    if (
      !this.event.Records ||
      !this.event.Records.length > 0 ||
      !this.event.Records[0].Sns
    ) {
      throw new Error("Malformed AWS SNS event");
    }
    this.message = JSON.parse(this.event.Records[0].Sns.Message);

    if (!typeof this.message === "object") {
      throw new Error("Json parse failed");
    }
    this.message = this.message.default;

    this.message.bucket = this.message.bucket
      .split(":")
      .reduce((reduced, part) => part); // put the last element into it
    /*
        this.content.default = {
            labels,
            bucket,
            key
          }
        */
  }
}

module.exports = EventFilter;
