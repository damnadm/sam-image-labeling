class Label {
  constructor(text, xMax = 250, yMax = 250) {
    this.text = text;
    this.suchifiers = ["wow", "such", "sooo", "many", "much", "very"];

    this.rotation =
      Math.round(Math.random() * 20) - Math.round(Math.random() * 20); // -10 < x < 10
    this.family = Math.round(Math.random() * 10) % 2 ? "sans" : "sans-serif";
    this.x = this._randomMinMax(0, xMax);
    this.y = this._randomMinMax(0, yMax);
    this.fill = `#${this._randCSSHex()}${this._randCSSHex()}${this._randCSSHex()}`;
    this.size = this._randomMinMax(10, 60);
  }

  get suchified() {
    const numOfPrefix = Math.round(Math.random() * 10) % this.suchifiers.length;
    return `${this.suchifiers[numOfPrefix]} ${this.text}`;
  }

  _randomMinMax(min, max) {
    return Math.round(Math.random() * (max - min)) + min;
  }

  _randCSSHex() {
    const num = parseInt(this._randomMinMax(0, 255), 10)
      .toString(16)
      .toUpperCase();
    return num.length < 2 ? `0${num}` : num;
  }
}

module.exports = Label;
