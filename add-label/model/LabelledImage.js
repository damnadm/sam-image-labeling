const Label = require("./Label");
const AWS = require("aws-sdk");
const S3 = new AWS.S3({ region: "eu-west-1" });
const gm = require("gm").subClass({ imageMagick: true });
const uuid = require("uuid/v1");

class LabelledImage {
  constructor(bucket, key, labels) {
    // label positioning needs the original image's width and height
    const xMax = 100;
    const yMax = 300;
    this.resultBucket = process.env.RESULT_BUCKET;
    this.bucket = bucket;
    this.key = key;
    this.labels = labels.map(label => new Label(label, xMax, yMax));
  }

  _readStreamFromS3() {
    return S3.getObject({
      Bucket: this.bucket,
      Key: this.key
    })
      .createReadStream()
      .on("error", error => {
        throw Error(
          "read stream error: " +
            " bucket: " +
            this.bucket +
            " message: " +
            error.message
        );
      });
  }

  async _upload(stream) {
    const bucket = this.resultBucket;
    console.log("start upload to " + bucket);
    const params = {
      Bucket: bucket,
      Key: uuid() + "-" + this.key,
      Body: stream,
      ContentType: "binary/octet-stream"
    };
    return S3.upload(params).promise();
  }

  async generateAndUpload() {
    let readableStream = this._readStreamFromS3();

    readableStream = gm(readableStream)
      .resize(400, 400)
      .stream();

    this.labels.forEach(label => {
      console.log("label params", label);
      readableStream = gm(readableStream)
        .fill(label.fill)
        .fontSize(label.size)
        .drawText(label.x, label.y, label.suchified)
        //  .rotate("white", label.labelParams.rotation)
        .stream();
    });

    try {
      console.log("write stream....");
      await this._upload(readableStream);
      console.log("written...");
    } catch (err) {
      console.error("Upload failed", err);
    }

    console.log("after upload");
  }
}

module.exports = LabelledImage;
