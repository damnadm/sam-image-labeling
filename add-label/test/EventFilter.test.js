const chai = require('chai')
const sinon = require('sinon')
const rewire = require('rewire')
const expect = chai.expect

const event = require('../test_events/event.json')

describe('EventFilter can get the important payload from an sns event', () => {
  describe('when initialized with a proper event', () => {
    let EventFilter
    beforeEach(() => {
       EventFilter = require('../model/EventFilter')
    })
    it('should have return an object with bucket, key, labels information', async () => {
      const eventInfos = new EventFilter(event)
      expect(eventInfos.message.key).to.equal('luca.png')
      expect(eventInfos.message.bucket).to.equal('arn:aws:s3:europe-west-1:342344234:label-dev')
      expect(eventInfos.message.labels.Labels.length).to.equal(2)
    })
  })
})