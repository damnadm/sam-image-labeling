const chai = require("chai");
const sinon = require("sinon");
const rewire = require("rewire");
const expect = chai.expect;

describe("Label can create a memeified text", () => {
  describe("when initialized with a string", () => {
    let Label;
    beforeEach(() => {
      Label = require("../model/Label");
    });
    it("should have return such memific result", async () => {
      const text = "shooze";
      const label = new Label(text);
      expect(label.suchified).to.match(/.+?shooze$/);
    });

    it("should have return svg with randomized properties and the memified label", async () => {
      const text = "shooze";
      const label = new Label(text, 0, 60);
      expect(label.suchified).to.match(/.+?shooze$/);
      expect(label.labelParams.x).to.equal(0);
      expect(label.labelParams.y).to.lessThan(60);
      expect(label.labelParams.family.length).to.greaterThan(0);
      expect(label.labelParams.rotation).to.lessThan(20);
      expect(label.labelParams.rotation).to.greaterThan(-20);
      expect(label.labelParams.fill.length).to.equal(7);
      expect(typeof label.SVG).to.equal("object");
      const resRegExp = /<svg><text x="\d+" y="\d+" fill="#[ABCDEF0-9]{6,6}" transform="rotate\((-|)\d+\)" font-family="(sans|sans-serif)">.*?\sshooze<\/text><\/svg>/;
      expect(label.SVG.svg).to.match(new RegExp(resRegExp));
    });
  });
});
