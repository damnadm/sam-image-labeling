const chai = require('chai')
const sinon = require('sinon')
const rewire = require('rewire')
const expect = chai.expect
const fs = require('fs');


describe('LabelledImage can create a labelled image', () => {
  describe('when initialized with a bucket, a key and an array of labels as strings', () => {
    let LabelledImage
    const targetFilePath = './test_events/uzsi-labelled.png'
    beforeEach(() => {
       LabelledImage = rewire('../model/LabelledImage')
    })
    after(() => {
      //fs.unlinkSync(targetFilePath)
    })
    it('should setup itself', () => {
      const labelledImage = new LabelledImage('labeling-dev', 'balint-luca.png', ['shoes', 'alcohol', 'water'])
      expect(labelledImage.labels.length).to.equal(3)
    })

    it('should generate an image', () => {
      LabelledImage.__set__({
        'S3': {
          getObject: (params) => {
            return {
              createReadStream: () => {
                console.log('cwd', process.cwd());
                return fs.createReadStream('./test_events/uzsi.png')
              }
            }
          },
          upload: (params) => {
            return {
              promise: () => {
                // params.Body is a passtrough stream. cwd is the root
                const target = fs.createWriteStream(targetFilePath)
                params.Body.pipe(target)
                return Promise.resolve()
              }
            }
          }
        }
      })
      const labelledImage = new LabelledImage('labeling-dev', 'uzsi.png', ['shoes', 'alcohol', 'water'])
      labelledImage.generateAndUpload()
      // check the existence of the target image
      //expect(() => fs.accessSync(targetFilePath)).to.not.throw();
    })



  })
})